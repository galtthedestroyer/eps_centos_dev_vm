#____ ____ ____ ___     ___ _  _ _ ____    ____ ____ ____ _ ___  ___    ____ _ ____ ____ ___   /
#|__/ |___ |__| |  \     |  |__| | [__     [__  |    |__/ | |__]  |     |___ | |__/ [__   |   / 
#|  \ |___ |  | |__/     |  |  | | ___]    ___] |___ |  \ | |     |     |    | |  \ ___]  |  .  

#0 reboot your machine and enable the virtual machine capabilities ASAP before they get locked out!!! ( in case you want to use a VM in the future. )

#1 on a BAH machine you must right click powershell and run as powerbroker administrator to run this script.  
# To do so you'll probably have to pin powershell to the start menu then drag a shortcut to the desktop.  The shortcut is right-clickable.

#2 You probably will have to manually make this script runnable by executing the following command in powershell:
# Set-ExectionPolicy Unrestricted
# or if windows says there is no such command run it without the parameter then type the parameter after it asks.

#3 install chocolatey for the rest of this script to work.  https://chocolatey.org/install#install-with-powershellexe
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#4 some BAH machines require FIPS compliant checksums
choco feature enable -n useFipsCompliantChecksums

#5 install programs
#all install / init commands are idempotent (it won't reinstall)

#choco upgrade firefox -y
#choco upgrade googlechrome -y

#choco upgrade minishift -y                #optional if you want to do openshift development on windows
choco upgrade aria2 -y                    #downloader
choco upgrade notepadplusplus.install -y  #without '.install' it installs as a portable app
choco upgrade ripgrep -y                  #fastest replacement for grep.  Also has full regex unlike grep.  (Grep can't do lazy searches even with egrep or -E.)
choco upgrade git.install -y
choco upgrade 7zip.install -y            #windows is missing any kind of "extract to..." menu dialog for archives

#you'll want to get the XMLTools for NP++ https://sourceforge.net/projects/npp-plugins/files/XML%20Tools/

#6 stop firefox from asking to add exceptions for every URL. i.e. install the same CA that your Microsoft browser has.
cd 'C:\Program Files\Mozilla Firefox\'
.\firefox.exe -new-tab 'http://10.250.14.34:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed'

# actual URL: http://ashbbcpsg01:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed

# maybe similar for chrome
#cd 'C:\Program Files (x86)\Google\Chrome\Application\'
#.\chrome.exe http://10.250.14.34:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed
