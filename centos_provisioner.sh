#!/usr/bin/env bash
  #enable newer versions of software. e.g. Maven
  yum install centos-release-scl
  # epel info from:  https://fedoraproject.org/wiki/EPEL
  yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
  yum clean all
  yum groupinstall -y "KDE Plasma Workspaces"

  #fastest replacement for grep.  Also has full regex unlike grep.  (Grep can't do lazy searches even with egrep or -E.)
  yum-config-manager --add-repo=https://copr.fedorainfracloud.org/coprs/carlwgeorge/ripgrep/repo/epel-7/carlwgeorge-ripgrep-epel-7.repo

# alphabetize this list!
# openshift and docker came from: https://wiki.centos.org/SpecialInterestGroup/PaaS/OpenShift-Quickstart so the remaining items are from that
# we use openshift 3.9 presently
#  centos-release-openshift-origin39
#  origin-clients
  yum install -y \
  aria2 \
  bash-completion \
  bind-utils \
  bridge-utils \
  docker \
  firefox \
  git \
  htop \
  iptables-services \
  kate \
  kde-runtime \
  net-tools \
  ripgrep \
  tmux \
  vim \
  wget \
  yum-plugin-versionlock \
  zsh

  #redhat only publishes docker that works with its current openshift so we can trust the version here -1.13.1-*
  #yum versionlock add docker
  #yum versionlock add centos-release-openshift-origin*

  # get openshift commandline tools
  #TMP_URL=`curl -L --compressed "https://github.com/openshift/origin/releases/latest" | grep -o 'href.*client.*linux-64bit\.tar\.gz'`
  # get rid of smallest match of everything before the first "/"
  #TMP_URL="${TMP_URL#*\/}"
  # get rid of the largest match of everything before the last "/"
  #oc_fname="${TMP_URL##*/}"
  #aria2c -o github_deleteme.tar.gz https://github.com/${TMP_URL}
  #tar -xf github_deleteme.tar.gz
  #cp github_deleteme/oc /usr/local/bin/oc
  #rm -rf github_deleteme*

  #edit /etc/sysconfig/docker file and add --insecure-registry 172.30.0.0/16 to the OPTIONS parameter.
  sed -i '/OPTIONS=.*/c\OPTIONS="--selinux-enabled --insecure-registry 172.30.0.0/16"' /etc/sysconfig/docker
  systemctl is-active docker
  systemctl enable docker

  #python 3 https://linuxize.com/post/how-to-install-python-3-on-centos-7/
  #yum might depend on python 2 so don't set 3 as default.
  yum install -y python36

  #install source to image
  #wget https://github.com/openshift/source-to-image/releases/download/v1.1.10/source-to-image-v1.1.10-27f0729d-linux-amd64.tar.gz
  #tar -fxz source-to-image-v1.1.10-27f0729d-linux-amd64.tar.gz -C /usr/local/bin
  #rm source-to-image-v1.1.10-27f0729d-linux-amd64.tar.gz

  #install latest source to image
  #TMP_URL=`curl -L --compressed "https://github.com/openshift/source-to-image/releases/latest" | grep -o 'href.*linux-amd64\.tar\.gz'`
  # get rid of smallest match of everything before the first "/"
  #TMP_URL="${TMP_URL#*\/}"
  # get rid of the largest match of everything before the last "/"
  #oc_fname="${TMP_URL##*/}"
  #aria2c -o github_deleteme.tar.gz https://github.com/${TMP_URL}
  #tar -xf github_deleteme.tar.gz
  #cp github_deleteme/* /usr/local/bin/
  #rm -rf github_deleteme*

  #install latest apache nifi
  #get best mirror first
  # TMP_URL=`curl -L --compressed "https://nifi.apache.org/download.html" | rg -N -o "https.*?bin.zip\"" | head -n 1`
  # TMP_URL=${TMP_URL%*\"}
  # TMP_URL=`curl -L --compressed $TMP_URL | rg -N -o "http.*?bin.zip\"" | head -n 1`
  # TMP_URL=${TMP_URL%*\"}
  # mkdir /opt/nifi
  # #nifi zip is 1.2GB so be careful about redownloading it
  # aria2c  --conditional-get --allow-overwrite -d /opt/nifi -o nifi.zip $TMP_URL
  # cd /opt/nifi
  # unzip nifi.zip
  # #install as a service https://nifi.apache.org/docs/nifi-docs/html/getting-started.html#installing-as-a-service
  # ./ni*/bin/nifi.sh install
  # rm nifi.zip
  # cd ~

  # make firefox work just like from within your host OS https://www.happyassassin.net/2015/01/14/trusting-additional-cas-in-fedora-rhel-centos-dont-append-to-etcpkitlscertsca-bundle-crt-or-etcpkitlscert-pem/
  #CERT URL provided by BAH help / support
  wget -t 2 -P /etc/pki/ca-trust/source/anchors/ http://ashbbcpsg01:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed
  update-ca-trust

  # add switchy omega to firefox to enable accessing "the IDE" which includes Jenkins
  ff_plugin=`curl -L --compressed "https://github.com/FelisCatus/SwitchyOmega/releases/latest" | grep -o 'href.*xpi'`
  ff_plugin=${ff_plugin#*\/}
  aria2c -d /usr/share/mozilla/extensions/* https://github.com/${ff_plugin}
  # todo this doesn't work bc this script is run as root.  see if plugins can be installed globally
  mkdir home/vagrant/.mozilla/firefox/*.default/browser-extension-data/switchyomega@feliscatus.addons.mozilla.org
  cp $(dirname $0)/switchyomega_settings.js home/vagrant/.mozilla/firefox/*.default/browser-extension-data/switchyomega@feliscatus.addons.mozilla.org/storage.js

  # add KDE Plasma integration to firefox
  #yum install -y plasma-browser-integration #requires QT5 which Centos 7.5 doesn't have
  #generic universal one-liner to download ff extension.  replace 'plasma-integration' with whatever shows up in the URL for your desired extension.  The sed part fixes slashes.
  #ff_plugin=`curl -L --compressed "https://addons.mozilla.org/en-US/firefox/addon/plasma-integration/?src=search" | sed "s^\\\u002F^/^g" | grep -E -o 'https:\/\/addons.mozilla.org\/firefox\/downloads\/file\/.*xpi'`
  #ff_plugin=${ff_plugin$$\?*}
  #aria2c -d /usr/share/mozilla/extensions/* ${ff_plugin}

  systemctl set-default graphical.target #default to gui.  otherwise: default.target

  # change KDE settings from the commandline!
  # find a setting in the GUI.  goto ~/.kde/share/config.  grep -rni [a useful word to locate which file has your setting].  look inside the file to find the group etc.
  # no '--file' means global.  These all overwrite, yet notice that it's OK to start with a comma.
  kwriteconfig                   --group Layout    --key Options     ,terminate:ctrl_alt_bksp # fastest way to kill all gui apps
  kwriteconfig --file kdeglobals --group General   --key ColorScheme 'Obsidian Coast'         # dark theme inside applications
  kwriteconfig --file plasmarc   --group Theme     --key name        oxygen                   # dark theme for shell widgets
  kwriteconfig --file oxygenrc   --group Windeco   --key ButtonSize  ButtonVeryLarge          # titlebar button size
