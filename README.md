# EPS_Centos_dev_vm

Windows script and Vagrantfile to automatically create a VM with Centos, KDE, Openshift, source-2-image, docker

Read both files before using them because there are some steps in the Powershell script that must be done before it can run.  

All of the files in this repo should be in the same directory.  The windows shell scripts should be run from within this directory while within this directory.

Also, you can change most of the options to your liking before running the scripts.  

The scripts to set up a linux VM probably won't work out of the box due to recent changes in VirtualBox and / or Centos.
In this event try to fix them or try to switch to the Kubuntu provisioner.  