____ ____ ____ ___     ___ _  _ _ ____    ____ ____ ____ _ ___  ___    ____ _ ____ ____ ___   /
|__/ |___ |__| |  \     |  |__| | [__     [__  |    |__/ | |__]  |     |___ | |__/ [__   |   / 
|  \ |___ |  | |__/     |  |  | | ___]    ___] |___ |  \ | |     |     |    | |  \ ___]  |  .  

#0 reboot your machine and enable the virtual machine capabilities ASAP before they get locked out!!!

#1 on a BAH machine you must right click powershell and run as powerbroker administrator to run this script.  
# To do so you'll probably have to pin powershell to the start menu then drag a shortcut to the desktop.  The shortcut is right-clickable.

#2 You probably will have to manually make this script runnable by executing the following command in powershell:
# Set-ExectionPolicy Unrestricted
# or if windows says there is no such command run it without the parameter then type the parameter after it asks.

#3 install chocolatey for the rest of this script to work.  https://chocolatey.org/install#install-with-powershellexe
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#4 some BAH machines require FIPS compliant checksums
choco feature enable -n useFipsCompliantChecksums

#5 install programs
#a. if you need a newer version but the choco package is still in moderation then specify the version on the command line
#all install / init commands are idempotent (it won't reinstall)
#b. https://status.chocolatey.org/
#possibly handy: https://download.virtualbox.org/virtualbox/LATEST.TXT and https://download.virtualbox.org/virtualbox/LATEST-STABLE.TXT returns e.g. 6.1.2
choco upgrade -y virtualbox --params "/NoDesktopShortcut /ExtensionPack"
choco upgrade -y vagrant

#choco upgrade firefox -y
#choco upgrade googlechrome -y
#choco upgrade minishift -y #optional if you want to do openshift development on windows
#choco upgrade notepadplusplus.install -y #without '.install' it installs as a portable app
#choco upgrade vim -y

#6 Oracle pulled usb 2 and 3 out of virtualbox and put them into an extension pack!  Or why not just set your vm to 1.1 in the vagrantfile?
# https://www.virtualbox.org/manual/ch01.html#intro-installing

# wget -OutFile Oracle_VM_VirtualBox_Extension_Pack-6.0.4.vbox-extpack https://download.virtualbox.org/virtualbox/6.0.4/Oracle_VM_VirtualBox_Extension_Pack-6.0.4.vbox-extpack
 
# VBoxManage extpack install --replace .\Oracle_VM_VirtualBox_Extension_Pack-6.0.4.vbox-extpack
 
#7 maybe reboot is necessary after installing vagrant

#The supplied vagrantfile must be in the same directory as this script.

#8 start the VM
vagrant plugin update vagrant-vbguest # keeps guest additions up to date with each 'up'
vagrant init
vagrant up

#9 in case virtualbox tries funny stuff with GUI resolution
VBoxManage setextradata global GUI/MaxGuestResolution any

#10 Create a Shortcut on the desktop http://www.devguru.com/content/technologies/wsh/objects-wshshortcut.html
$TargetFile = "$env:SystemRoot\HashiCorp\Vagrant\bin\vagrant.exe"
$ShortcutFile = "$env:USERPROFILE\Desktop\Centos_Dev.lnk"
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut($ShortcutFile)
$Shortcut.TargetPath = $TargetFile
$Shortcut.Arguments = "up"
$Shortcut.WorkingDirectory = (Get-Location).tostring()
$ShortCut.Description = "Linux Dev Env"
$ShortCut.IconLocation = "$env:ProgramFiles\Oracle\VirtualBox\VirtualBox.exe, 0"
$ShortCut.WindowStyle = 7
$Shortcut.Save()

#2018-08-21 this is moved to a trigger in the vagrantfile, but a vagrant bug requires that I leave it here for now.
VBoxManage controlvm "centos7_5_dev_environment" setvideomodehint 1920 1080 32

#VBoxManage setextradata global GUI/Fullscreen         on   # or "vm name" instead of global

#might have to add user to vboxsf group
