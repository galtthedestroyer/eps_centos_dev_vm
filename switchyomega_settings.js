{
    "+adx3": {
        "bypassList": [
            {
                "conditionType": "BypassCondition",
                "pattern": "127.0.0.1"
            },
            {
                "conditionType": "BypassCondition",
                "pattern": "::1"
            },
            {
                "conditionType": "BypassCondition",
                "pattern": "localhost"
            }
        ],
        "color": "#99ccee",
        "fallbackProxy": {
            "host": "127.0.0.1",
            "port": 9999,
            "scheme": "socks5"
        },
        "name": "adx3",
        "profileType": "FixedProfile",
        "revision": "1666458eae4"
    },
    "+auto switch": {
        "color": "#99dd99",
        "defaultProfileName": "direct",
        "name": "auto switch",
        "profileType": "SwitchProfile",
        "revision": "16682a8311f",
        "rules": [
            {
                "condition": {
                    "conditionType": "HostWildcardCondition",
                    "pattern": "*.dicelab.net"
                },
                "profileName": "direct"
            },
            {
                "condition": {
                    "conditionType": "HostWildcardCondition",
                    "pattern": "*.adx3.org*"
                },
                "profileName": "adx3"
            }
        ]
    },
    "-addConditionsToBottom": false,
    "-confirmDeletion": true,
    "-downloadInterval": 1440,
    "-enableQuickSwitch": false,
    "-exportLegacyRuleList": false,
    "-monitorWebRequests": true,
    "-quickSwitchProfiles": [],
    "-refreshOnProfileChange": true,
    "-revertProxyChanges": true,
    "-showExternalProfile": true,
    "-showInspectMenu": true,
    "-startupProfileName": "",
    "schemaVersion": 2
}
